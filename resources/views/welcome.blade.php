<!DOCTYPE html>
<html lang="en">
<head>
  <title>BDTASK</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>


</head>
<body>

<div class="container" id= "app">
  <h3>USER CRUD USING VUE</h3>

  <button class="btn btn-sm btn-primary pull-right" @click="create">+Add New User</button>
  <br/><br/>

 <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
      <tr>
        <th>User Name</th>
        <th>Email</th>
        <th>Contact</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

      <tr v-for="row in data">
        <td>@{{row.name}}</td>
        <td>@{{row.email}}</td>
        <td>@{{row.contact}}</td>
        <td>

          <button class="btn btn-warning btn-sm" @click="edit(row)">Edit</button>
          <button class="btn btn-danger btn-sm" @click="deleteData(row)">Delete</button>

        </td>
      </tr>

    </tbody>
  </table>

<div class="modal fade" id="modal">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@{{ isInsert ? 'New User' : 'Edit User'  }}</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="" v-model="User.name">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" name="" v-model="User.email">
        </div>
        <div class="form-group">
          <label>Conrtact</label>
          <input type="text" class="form-control" name="" v-model="User.contact">
        </div>

        <div class="model-footer">

          <button v-if="isInsert" type="submit" class="btn btn-primary btn-sm" @click="postData(User)">Save</button>
          <button v-if="!isInsert" type="submit" class="btn btn-primary btn-sm" @click="updateData(User)">Update</button>

        </div>


      </div>
    </div>
  </div>
</div>

</div>

<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script>
  var csrfToken = '{{csrf_token()}}';
  var userInfoUrl = '{{ url('userdata') }}';
  var updateuserInfoUrl = '{{ url('userdataUpdate') }}';
  var deleteuserInfoUrl = '{{ url('userdatadelete') }}';

  var app = new Vue({
    el : "#app",
    data: {
      data: [],
      isInsert: true,
      User: {'name':null,'email':null,'contact':null,'id':null}
    },
    created: function(){
      this.init();
    },
    methods: {

      init: function()
      {
        axios.get(userInfoUrl)
                .then(response => {
                    this.data = response.data;
                });

        // this.$http.get(userInfoUrl)
        //         .then(function(response) {
        //             this.data = response.data;
        //         });
      },

      create: function()
      {
         this.isInsert = true,
         this.User = {},
         $("#modal").modal("show")
      },
      postData(data){
        if(!confirm('Are you sure?')) return;
        data._token = this.csrfToken;
        axios.post(userInfoUrl, data)
              .then(response => {
                     this.init();
                      $("#modal").modal("hide");
                });
      },
      edit(data){
        this.isInsert = false;
        $("#modal").modal("show");
        this.User = data;

      },

      updateData(data){
        data._token = this.csrfToken;
        axios.post(updateuserInfoUrl, data)
              .then(response => {
                     this.init();
                      $("#modal").modal("hide");
                });
      },

      deleteData(data){

        if(!confirm('Are you sure?')) return;
        data._token = this.csrfToken;
        axios.post(deleteuserInfoUrl, data)
              .then(response => {
                     this.init();
                });
      }



    }


  });

</script>

<!-- <script>
  $(document).ready(function() {
    $('#example').DataTable();
});
</script> -->

</body>
</html>
