<?php

use Illuminate\Database\Seeder;
use App\Employee;


class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

		factory(App\Employee::class,50)->create();
    }
}
