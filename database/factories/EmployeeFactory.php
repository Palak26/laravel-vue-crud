<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
     return [
        //
		    'emp_name' => $faker->name,
		    'email' => $faker->email,
		    'contact' => '0167123'.rand(1111,9999),
		    'department' => 'IT',
		    'emp_type' => 'Permanent',
		    'emp_grade_code' => 'GS-'.rand(1,5),
		    'status' => 1
    ];
});
