<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function getData()
    {
        $user = User::all();
        return $user;
    }

    public function store(Request $request)
    {
        User::create($request->all());
        return response()->json(['status'=>200, 'message'=>'successfully data stored']);
    }

    public function update(Request $request)
    {
        $user =  User::find($request->id);
        $user->update($request->all());
        return response()->json(['status'=>200, 'message'=>'successfully data updated']);
    }

    public function destroy(Request $request)
    {
       $user =  User::find($request->id);
       $user->delete();
       return response()->json(['status'=>200, 'message'=>'successfully data deleted']);
    }
}
